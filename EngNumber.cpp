/* EngNumber
   A small library for converting floats to engineering notation.
   Intended for use with fixed-size displays.

   Based on Rob Tillaart's printFloat enhancements in
   http://forum.arduino.cc/index.php?topic=179111.0

   Copyright (c) 2008 David A. Mellis.  All right reserved.

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with this library; if not, write to the Free Software
   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/
#include <inttypes.h>
#include <Arduino.h>

#include "EngNumber.h"

#define divmod10_asm(in32, mod8, tmp8)                      \
  asm volatile (                                            \
                " ldi %2,51     \n\t"                       \
                " mul %A0,%2    \n\t"                       \
                " clr %A0       \n\t"                       \
                " add r0,%2     \n\t"                       \
                " adc %A0,r1    \n\t"                       \
                " mov %1,r0     \n\t"                       \
                " mul %B0,%2    \n\t"                       \
                " clr %B0       \n\t"                       \
                " add %A0,r0    \n\t"                       \
                " adc %B0,r1    \n\t"                       \
                " mul %C0,%2    \n\t"                       \
                " clr %C0       \n\t"                       \
                " add %B0,r0    \n\t"                       \
                " adc %C0,r1    \n\t"                       \
                " mul %D0,%2    \n\t"                       \
                " clr %D0       \n\t"                       \
                " add %C0,r0    \n\t"                       \
                " adc %D0,r1    \n\t"                       \
                " clr r1        \n\t"                       \
                " add %1,%A0    \n\t"                       \
                " adc %A0,%B0   \n\t"                       \
                " adc %B0,%C0   \n\t"                       \
                " adc %C0,%D0   \n\t"                       \
                " adc %D0,r1    \n\t"                       \
                " add %1,%B0    \n\t"                       \
                " adc %A0,%C0   \n\t"                       \
                " adc %B0,%D0   \n\t"                       \
                " adc %C0,r1    \n\t"                       \
                " adc %D0,r1    \n\t"                       \
                " add %1,%D0    \n\t"                       \
                " adc %A0,r1    \n\t"                       \
                " adc %B0,r1    \n\t"                       \
                " adc %C0,r1    \n\t"                       \
                " adc %D0,r1    \n\t"                       \
                " lsr %D0       \n\t"                       \
                " ror %C0       \n\t"                       \
                " ror %B0       \n\t"                       \
                " ror %A0       \n\t"                       \
                " ror %1        \n\t"                       \
                " ldi %2,10     \n\t"                       \
                " mul %1,%2     \n\t"                       \
                " mov %1,r1     \n\t"                       \
                " clr r1        \n\t"                       \
                :"+r"(in32),"=d"(mod8),"=d"(tmp8) : : "r0")

void floatToEng(double number, EngNumber *engnum) {
  uint8_t digits = ENGNUM_DIGITS;

  for (int i=0; i<ENGNUM_DIGITS; i++) {
    engnum->digits[i] = 0;
  }

  // index of the digits array
  int idx = 0;

  size_t n = 0;
  engnum->negative = signbit(number);
  if (engnum->negative) {
    number = -number;
  }

  digits = max(3, digits);
  digits = min(digits, ENGNUM_FLOAT_MAX_DIGITS);

  engnum->exponent = 0;
  while (number >= 1000.0) {
    number *= 0.001;
    engnum->exponent += 3;
  }
  if (number != 0) {
    while (number < 1.0) {
      number *= 1000.0;
      engnum->exponent -= 3;
    }
  }

  int decimals = digits;
  if (number >= 100.0) decimals = digits - 3;
  else if (number >= 10.0) decimals = digits - 2;
  else decimals = digits - 1;

  if (decimals <= ENGNUM_FLOAT_MAX_DIGITS) {
    const double rounding[ENGNUM_FLOAT_MAX_DIGITS + 1] =
      { 5E-1, 5E-2, 5E-3, 5E-4, 5E-5, 5E-6, 5E-7, 5E-8 };
    number += rounding[decimals];
  } else {
    double rounding = 5E-9;
    // optimize stepsize=2 ==> *0.01
    for (uint8_t i=8; i < decimals; ++i) {
      rounding *= 0.1;
    }
    number += rounding;
  }

  // fix 'overflow' due to rounding
  if (number >= 1000.0) {
    number *= 0.001;
    engnum->exponent += 3;
  }

  // Extract the integer part of the number and print it
  unsigned long int_part = (unsigned long)number;

  {
    uint32_t rem = int_part;
    uint8_t int_buf[ENGNUM_DIGITS];
    uint8_t int_idx = ENGNUM_DIGITS-1;

    uint32_t d;
    uint8_t m;
    while (rem >= 10) {
      divmod10_asm(rem, m, d);
      int_buf[int_idx] = m;
      int_idx--;
    }
    int_buf[int_idx] = rem; // insert last digit directly

    while (int_idx != ENGNUM_DIGITS) {
      engnum->digits[idx] = int_buf[int_idx];
      idx++;
      int_idx++;
    }
  }
  // dp should be set on the digit before
  engnum->dp = idx-1;
  if (digits <= idx) digits = 0;
  else digits -= idx;

  // Print the decimal part
  if (digits > 0) {
    double remainder = number - int_part;

    // to allow original layouts
    if (digits > ENGNUM_FLOAT_MAX_DIGITS) {
      while (digits-- > 0) {
        remainder *= 10.0;
        uint8_t toPrint = remainder; // tweaked uint8_t here
        remainder -= toPrint;
      }
    } else { // fast formatting.
      // make an unsigned long of the decimal part
      const double remMult[ENGNUM_FLOAT_MAX_DIGITS] =
        { 1E1, 1E2, 1E3, 1E4, 1E5, 1E6, 1E7 };  // , 1E8, 1E9 };
      uint32_t rem = remainder * remMult[digits-1];

      uint8_t dec_buf[ENGNUM_DIGITS];
      uint8_t dec_idx = ENGNUM_DIGITS-1;

      uint32_t d;
      uint8_t m;
      while (rem >= 10) {
        divmod10_asm(rem, m, d);
        dec_buf[dec_idx] = m;
        dec_idx--;
      }
      dec_buf[dec_idx] = rem; // insert last digit directly

      while (dec_idx != ENGNUM_DIGITS) {
        engnum->digits[idx] = dec_buf[dec_idx];
        idx++;
        dec_idx++;
      }
    }
  }
}
