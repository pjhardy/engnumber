/* EngNumber
   A small library for converting floats to engineering notation.
   Intended for use with fixed-size displays.

   Based on Rob Tillaart's printFloat enhancements in
   http://forum.arduino.cc/index.php?topic=179111.0

   Copyright (c) 2008 David A. Mellis.  All right reserved.

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with this library; if not, write to the Free Software
   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#define ENGNUM_DIGITS 4
#define ENGNUM_FLOAT_MAX_DIGITS 7

struct EngNumber {
  bool negative;
  int digits[ENGNUM_DIGITS];
  int dp;
  int exponent;
};

void floatToEng(double number, EngNumber *engnum);
