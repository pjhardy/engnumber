EngNumber
=========

An Arduino library for converting floats to engineering notation with fixed
precision. Intended for use with fixed-size displays.

Based on Rob Tillaart's printFloat enhancements in
http://forum.arduino.cc/index.php?topic=179111.0

Usage
-----

* Set the `ENGNUM_DIGITS` to the number of significant digits required.
* Create an EngNumber variable to act as a buffer.
    EngNumber buf;
* The `floatToEng` method can be used to convert a float and store it
in an EngNumber variable.
~~~~
float x = 12345678.90;
floatToEng(x, &buf);
~~~~

The EngNumber struct contains a few elements describing the input float.

* `bool negative`. True if the float is negative.
* `int digits[ENGNUM_DIGITS]`. An array of integers representing digits of
the float.
* `int dp`. Index of the decimal point in the final engineering notation
number. `digits[dp]` should have a decimal point after it.
* `int exponent`. The exponent part of the engineering notation number.
This will always be a multiple of 3.
