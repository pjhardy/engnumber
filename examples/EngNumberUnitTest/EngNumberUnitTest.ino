/* EngNumberUnitTest
   Testing the EngNumber library with the arduinounit unit testing framework.
   Requires arduinounit library ( https://github.com/mmurdoch/arduinounit ).

   Peter Hardy
   <peter@hardy.dropbear.id.au>
*/
#include <EngNumber.h>
#include <ArduinoUnit.h>

test(basic_conversion)
{
  // Asserting 1234 is converted to 1.234*10^3
  float x = 1234;
  EngNumber testEngNum;
  floatToEng(x, &testEngNum);

  assertEqual(testEngNum.negative, false);
  assertEqual(testEngNum.digits[0], 1);
  assertEqual(testEngNum.digits[1], 2);
  assertEqual(testEngNum.digits[2], 3);
  assertEqual(testEngNum.digits[3], 4);
  assertEqual(testEngNum.dp, 0);
  assertEqual(testEngNum.exponent, 3);
}

test(small_positive)
{
  // Asserting 0.0001234 is converted to 123.4*10^-6
  float x = 0.0001234;
  EngNumber testEngNum;
  floatToEng(x, &testEngNum);

  assertEqual(testEngNum.negative, false);
  assertEqual(testEngNum.digits[0], 1);
  assertEqual(testEngNum.digits[1], 2);
  assertEqual(testEngNum.digits[2], 3);
  assertEqual(testEngNum.digits[3], 4);
  assertEqual(testEngNum.dp, 2);
  assertEqual(testEngNum.exponent, -6);
}

test(single_digit)
{
  // Asserting 1 is converted to 1.000*10^0
  float x = 1;
  EngNumber testEngNum;
  floatToEng(x, &testEngNum);

  assertEqual(testEngNum.negative, false);
  assertEqual(testEngNum.digits[0], 1);
  assertEqual(testEngNum.digits[1], 0);
  assertEqual(testEngNum.digits[2], 0);
  assertEqual(testEngNum.digits[3], 0);
  assertEqual(testEngNum.dp, 0);
  assertEqual(testEngNum.exponent, 0);
}

test(small_negative)
{
  // Asserting -0.1234 is converted to -123.4*10^-3
  float x = -0.1234;
  EngNumber testEngNum;
  floatToEng(x, &testEngNum);

  assertEqual(testEngNum.negative, true);
  assertEqual(testEngNum.digits[0], 1);
  assertEqual(testEngNum.digits[1], 2);
  assertEqual(testEngNum.digits[2], 3);
  assertEqual(testEngNum.digits[3], 4);
  assertEqual(testEngNum.dp, 2);
  assertEqual(testEngNum.exponent, -3);
}

test(large_negative)
{
  // Asserting -12345678900 is converted to -12.35*10^9
  // Rounding works!
  float x = -12345678900;
  EngNumber testEngNum;
  floatToEng(x, &testEngNum);

  assertEqual(testEngNum.negative, true);
  assertEqual(testEngNum.digits[0], 1);
  assertEqual(testEngNum.digits[1], 2);
  assertEqual(testEngNum.digits[2], 3);
  assertEqual(testEngNum.digits[3], 5);
  assertEqual(testEngNum.dp, 1);
  assertEqual(testEngNum.exponent, 9);
}

test(zero_tenth_small)
{
  // Asserting that 1.02345 is converted to 1.023*10^1
  float x = 1.02345;
  EngNumber testEngNum;
  floatToEng(x, &testEngNum);

  assertEqual(testEngNum.negative, false);
  assertEqual(testEngNum.digits[0], 1);
  assertEqual(testEngNum.digits[1], 0);
  assertEqual(testEngNum.digits[2], 2);
  assertEqual(testEngNum.digits[3], 3);
  assertEqual(testEngNum.dp, 1);
  assertEqual(testEngNum.exponent, 1);
}

test(zero_tenth)
{
  // Asserting that 12.0345 is converted to 12.03*10^1
  float x = 12.0345;
  EngNumber testEngNum;
  floatToEng(x, &testEngNum);

  assertEqual(testEngNum.negative, false);
  assertEqual(testEngNum.digits[0], 1);
  assertEqual(testEngNum.digits[1], 2);
  assertEqual(testEngNum.digits[2], 0);
  assertEqual(testEngNum.digits[3], 3);
  assertEqual(testEngNum.dp, 1);
  assertEqual(testEngNum.exponent, 1);
}

test(zero_tenth_large)
{
  // Asserting that 1023456 is converted to 1.023*10^3
  float x = 1023456;
  EngNumber testEngNum;
  floatToEng(x, &testEngNum);

  assertEqual(testEngNum.negative, false);
  assertEqual(testEngNum.digits[0], 1);
  assertEqual(testEngNum.digits[1], 0);
  assertEqual(testEngNum.digits[2], 2);
  assertEqual(testEngNum.digits[3], 3);
  assertEqual(testEngNum.dp, 0);
  assertEqual(testEngNum.exponent, 3);
}

test(zero_hundredth)
{
  // Asserting that 1.20345 is converted to 1.203*10^1
  float x = 1.20345;
  EngNumber testEngNum;
  floatToEng(x, &testEngNum);

  assertEqual(testEngNum.negative, false);
  assertEqual(testEngNum.digits[0], 1);
  assertEqual(testEngNum.digits[1], 2);
  assertEqual(testEngNum.digits[2], 0);
  assertEqual(testEngNum.digits[3], 3);
  assertEqual(testEngNum.dp, 0);
  assertEqual(testEngNum.exponent, 1);
}

test(two_zeroes)
{
  // Asserting that 1.002345 is converted to 1.002*10^1
  float x = 1.002345;
  EngNumber testEngNum;
  floatToEng(x, &testEngNum);

  assertEqual(testEngNum.negative, false);
  assertEqual(testEngNum.digits[0], 1);
  assertEqual(testEngNum.digits[1], 0);
  assertEqual(testEngNum.digits[2], 0);
  assertEqual(testEngNum.digits[3], 2);
  assertEqual(testEngNum.dp, 0);
  assertEqual(testEngNum.exponent, 1);
}

void setup() {
  delay(2000); // Let everything settle down before starting
  Serial.begin(9600);
  while(!Serial); // Leonardo needs some time to think about things
  Serial.println();
  Serial.println("EngNumberUnitTest starting...");
}

void loop() {
  Test::run();
}
