/* EngNumberTest
   Basic test of the EngNumber library capabilities
*/
#include <EngNumber.h>

void setup() {
  Serial.begin(9600);
  delay(5000);
  Serial.println("EngNumberTest starting");

  EngNumber testNum;
  float x = 123.456;
  floatToEng(x, &testNum);
}

void loop() {
  // Nothing here
}
